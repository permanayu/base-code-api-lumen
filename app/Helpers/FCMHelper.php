<?php 
/** 
 * Auth Helpers
 * 
 * Updated 11 Juni 2020, 09:40
 *
 * @author Yudi Setiadi Permana 
 *
 */
namespace App\Helpers;

use App\Models\FCMRegistration;

class FCMHelper{
	
	/**
	 * Save Token
	 *
	 * @param
	 * - token 		: token fcm
	 * - user 		: user id
	 * - client_id 	: client id for auth verification
	 * 
	 * @return string
	 *
	 */
	public function saveToken($token, $user, $clientId){

		$data 	= array(
					'fcm_reg_token'		=> $token,
					'fcm_reg_device'	=> '',
					'auth_client_id'	=> $clientId,
					'user_id'			=> $user,
					'created_by'		=> $user,
				);

		$existToken = FCMRegistration::where('auth_client_id', $clientId)->where('user_id', $user)->first();

		if ($existToken) {
			// Update token
			$existToken->update($data);
		}
		else{
			// Insert new token
			FCMRegistration::create($data);
		}

		return $token;

	}
	
}