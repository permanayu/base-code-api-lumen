<?php 
/** 
 * Auth Helpers
 * 
 * Updated 11 Mei 2021, 09:40
 *
 * @author Yudi Setiadi Permana 
 *
 */
namespace App\Helpers;

use Illuminate\Http\Response;
use App\Models\ApiAuth;
use App\Models\ApiToken;
use App\Helpers\ResponseHelper;
use App\Helpers\RequestHelper;

class AuthHelper{

	public $userId;
	public $userType;

	public function __construct(){

        $this->_requestHelper = new RequestHelper;

    }

	/**
	 * Verify api auth request
	 *
	 * @param
	 * - type  	: 1 = normal request, 2 = user verify
	 * - msg 	: message for response api
	 * - result : data resutl for response api
	 * 
	 * @return json
	 *
	 */
	public function check($type = 1, $jwt){

		$params = $this->_requestHelper->getRequestParams($jwt); 

		// Check Client
		if (empty($params['client_id']) || empty($params['client_secret'])) {
			return ResponseHelper::setErrorResponse('Missing auth client');
		}

		// Verify API client
		$clientId       = $params['client_id'];
        $clientSecret   = $params['client_secret'];

        $verifyClient 	= ApiAuth::where('auth_client_id', $clientId)->where('auth_client_secret', $clientSecret)->first();

        if (!$verifyClient) {
        	return ResponseHelper::setErrorResponse('Invalid Client Id Or Client Secret');
        }

        // Verify API token
        if ($type == 2) {

        	if (empty($params['token'])) {
        		return ResponseHelper::setErrorResponse('Authentication Failed');
        	}

        	$verifyToken = ApiToken::where('token_id', $params['token'])->where('auth_client_id', $clientId)->first();

        	if (!$verifyToken) {
	        	return ResponseHelper::setErrorResponse('Invalid token');
	        }

	        $this->userId 	= $verifyToken->user_id;

        }

	}

	/**
	 * Generate token
	 *
	 * @param
	 * - type 		: 1 = student, 2 = teacher
	 * - user 		: user id
	 * - client_id 	: client id for auth verification
	 * 
	 * @return string
	 *
	 */
	public function generateToken($user, $clientId){

		$tokenModel = new ApiToken;
		$token 		= $tokenModel->generate();

		$data 	= array(
					'token_id'			=> $token,
					'auth_client_id'	=> $clientId,
					'user_id'			=> $user,
					'created_by'		=> $user,
					'token_time'		=> date('Y-m-d H:i:s'),
					'token_expired_time'=> date('Y-m-d H:i:s', time() + 2592000)
				);

		$existToken = ApiToken::where('auth_client_id', $clientId)->where('user_id', $user)->first();

		if ($existToken) {
			// Update token
			$existToken->update($data);
		}
		else{
			// Insert new token
			ApiToken::create($data);
		}

		return $token;

	}

}

?>