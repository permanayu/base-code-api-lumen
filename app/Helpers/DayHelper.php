<?php

namespace App\Helpers;

class DayHelper {

    /**
     * Method getDay
     * @param $day
     * 
     * @return string
     */
    public static function getDay($day)
    {
        $res = "";

        switch ($day) { 
            case 1: $res = "Senin"; break;
            case 2: $res = "Selesa"; break;
            case 3: $res = "Rabu"; break;
            case 4: $res = "Kamis"; break;
            case 5: $res = "Jumat"; break;
            case 6: $res = "Sabtu"; break;
            case 6: $res = "Minggu"; break;
        }

        return $res;
    }

    /**
     * Method getSplitTime
     * @param $time
     * 
     * @return string
     */
    public static function getSplitTime($time)
    {
        return substr($time, 0, 2) .':'. substr($time, 2, 2);
    }
}
