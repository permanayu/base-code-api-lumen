<?php 
/** 
 * Response Helpers
 * 
 * Updated 11 Juni 2020, 09:40
 *
 * @author Yudi Setiadi Permana 
 *
 */
namespace App\Helpers;

use Firebase\JWT\JWT;

class RequestHelper{

	/**
	 * Get Request Params Decode JWT
	 *
	 * @param
	 * - jwt 	: jwt encoded
	 *
	 * @return array
	 *
	 */
	public function getRequestParams($jwt){

		// Decode JWT
		if (empty($jwt['jwt'])) {
			return ResponseHelper::setErrorResponse('Invalid params request');
		}

		$jwtDecode 	= JWT::decode($jwt['jwt'], env('JWT_KEY'), array('HS256'));
		$params 	= (array) $jwtDecode; 

		return $params;

	}
	
}

?>