<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Helpers\RequestHelper;
use App\Helpers\ResponseHelper;
use App\Helpers\AuthHelper;
use App\Helpers\FCMHelper;

use App\Models\ApiToken;
use App\Models\User;
use App\Models\UserRole;

use Log;

class UserController extends Controller
{

    public function __construct()
    {
        $this->_authHelper      = new AuthHelper;
        $this->_fcmHelper       = new FCMHelper;
        $this->_requestHelper   = new RequestHelper;
    }

    /**
     * Authentification for user login to apps.
     *
     * @param 
     * - string  $client_id     : ID klien yang diberikan untuk Auth API
     * - string  $client_secret : Kunci klien yang diberikan untuk Auth API
     * - string  $usernmae      : Name pengguna yang digunakan
     * - string  $password      : Kata sandi yang digunakan
     * - string  $fcm_token     : Register id atau token yang digunakan untuk fcm
     *
     * @return object json 
     */
    public function login(Request $request){

        // Verify Client
        $this->_authHelper->check(1, $request->all());

        $params = $this->_requestHelper->getRequestParams($request->all());

        if (empty($params['username']) || empty($params['password']) || empty($params['fcm_token'])) {
            return ResponseHelper::setErrorResponse('Data tidak lengkap');
        }

        $result = array();

        // Get User
        $getUser = User::where('user_username', $params['username'])->first();

        if (!$getUser) {
            return ResponseHelper::setErrorResponse('Akun tidak ditemukan');
        }

        // Check Password
        if (!Hash::check($params['password'], $getUser->user_password)) {
             return ResponseHelper::setErrorResponse('Kata sandi tidak cocok');
        }

        $token  = $this->_authHelper->generateToken($getUser->user_id, $params['client_id']);

        // Save token FCM
        $this->_fcmHelper->saveToken($params['fcm_token'], $getUser->user_id, $params['client_id']);

        $result = array(
                    'username'  => $getUser->user_user_name,
                    'name'      => $getUser->user_name,
                    'email'     => $getUser->user_email,
                    'token'     => $token
                );

        return ResponseHelper::setResponse('Login berhasil', $result);

    }

    /**
     * Delete user token.
     *
     * @param 
     * - string  $client_id     : ID klien yang diberikan untuk Auth API
     * - string  $client_secret : Kunci klien yang diberikan untuk Auth API
     * - string  $token         : Token yang didapatkan pada saat login
     *
     * @return object json 
     */
    public function logout(Request $request){

        // Verify Client
        $this->_authHelper->check(2, $request->all());

        $params = $this->_requestHelper->getRequestParams($request->all());
        
        if (empty($params['token'])) {
            return ResponseHelper::setErrorResponse('Invalid token');
        }

        // Delete token
        ApiToken::where('token_id', $params['token'])->delete();

        return ResponseHelper::setResponse('Token berhasil dihapus', null);
    }

}