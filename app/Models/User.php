<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table        = "sys_users";
    protected $primaryKey   = "user_id";
    protected $fillable     = ['user_name', 'user_email', 'user_password', 'created_at', 'created_by', 'updated_at', 'updated_by'];
}