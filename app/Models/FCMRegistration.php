<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FCMRegistration extends Model
{
    protected $table        = "fcm_registrations";
    protected $primaryKey   = "fcm_id";
    protected $fillable     = ['fcm_reg_token', 'fcm_reg_device', 'auth_client_id', 'user_id', 'created_by', 'created_at', 'updated_at'];
}