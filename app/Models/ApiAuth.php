<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiAuth extends Model
{
    protected $table        = "api_auths";
    protected $primaryKey   = "auth_client_id";
    protected $fillable     = ['auth_client_id', 'auth_client_secret', 'created_at', 'created_by', 'updated_at', 'updated_by'];
}