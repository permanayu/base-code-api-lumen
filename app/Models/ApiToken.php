<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiToken extends Model
{
    protected $table        = "api_tokens";
    protected $primaryKey   = "token_id";
    protected $fillable     = ['token_id', 'auth_client_id', 'user_id', 'token_time', 'token_expired_time', 'created_by', 'created_at', 'updated_at'];

    public function generate(){

        do{

            $token = sha1(uniqid() . time());

            $query = DB::table($this->table)
                        ->where('token_id', $token)
                        ->first();

        } while ($query);

        return $token;

    }
}