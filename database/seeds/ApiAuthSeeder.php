<?php

use Illuminate\Database\Seeder;

class ApiAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_auths')->insert([
            [
                'auth_client_id'        => 'ANDROID_SAMPLE',
                'auth_client_secret'    => '7621eb99983e23f0920b646ddd85aa7396ddd8a4',
                'created_at'            => date('Y-m-d H:i:s'),
                'created_by'            => '1'
            ],
            [
                'auth_client_id'        => 'IOS_SAMPLE',
                'auth_client_secret'    => 'ca82e6244786e78abe27a31d43406ed61cf69da4',
                'created_at'            => date('Y-m-d H:i:s'),
                'created_by'            => '1'
            ]
        ]);
    }
}

