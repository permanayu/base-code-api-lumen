<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFcmRegistrationUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fcm_registrations', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned();
            $table->string('auth_client_id', 50);

            $table->foreign('user_id')
                ->references('user_id')
                ->on('sys_users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('auth_client_id')
                ->references('auth_client_id')
                ->on('api_auths')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fcm_registrations', function($table) {
            $table->dropColumn('user_id');
            $table->dropColumn('auth_client_id');
        });
    }
}
