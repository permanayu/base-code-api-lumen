<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_menus', function (Blueprint $table) {
            $table->bigIncrements('menu_id');
            $table->bigInteger('module_id')->nullable()->unsigned();
            $table->string('menu_name', 100);
            $table->string('menu_url', 100);
            $table->string('menu_icon', 50)->nullable();
            $table->smallInteger('menu_is_sub');
            $table->bigInteger('menu_parent_id')->nullable()->unsigned();
            $table->smallInteger('menu_position');
            $table->dateTime('created_at');
            $table->bigInteger('created_by')->unsigned();
            $table->dateTime('updated_at')->nullable();
            $table->bigInteger('updated_by')->nullable()->unsigned();

            $table->foreign('created_by')
                ->references('user_id')
                ->on('sys_users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('updated_by')
                ->references('user_id')
                ->on('sys_users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('module_id')
                ->references('module_id')
                ->on('sys_modules')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_menus');
    }
}
