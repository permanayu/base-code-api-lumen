<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fcm_logs', function (Blueprint $table) {
            $table->bigIncrements('fcm_log_id');
            $table->text('fcm_reg_token');
            $table->text('fcm_log_url');
            $table->text('fcm_log_data')->nullable();
            $table->smallInteger('fcm_log_status')->default(1);
            $table->text('fcm_log_response')->nullable();
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fcm_logs');
    }
}
