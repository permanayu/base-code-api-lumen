<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) { return 'API Itenas Apps'; });

$router->post('user/login',         'UserController@login');
$router->get('user/profile',        'UserController@profile');
$router->post('user/logout',        'UserController@logout');

$router->get('schedule',            'ScheduleController@index');
$router->get('schedule/detail',     'ScheduleController@detail');

$router->post('attendance/scan',    'AttendanceController@scan');
$router->get('attendance/history',  'AttendanceController@history');
$router->get('attendance/history/lecture',  'AttendanceController@historyLecture');
$router->post('attendance/start',  	'AttendanceController@startCourse');

$router->get('lecturenews', 		'LectureNewsController@index');
$router->get('lecturenews/detail', 	'LectureNewsController@show');
$router->post('lecturenews/save', 	'LectureNewsController@save');

$router->get('notification', 		'NotificationController@index');
$router->post('notification/read', 	'NotificationController@read');


