<?php

/**
 * Configurasi document.
 *
 * @author Yudi Setiadi Permana
 */

/* Main config */
$config   = array(
				// 'url'		=> 'http://', // Production Server
				// 'url'		=> 'http://', // Development Server
				'url'		=> 'http://localhost/base-api-lumen/public', // Local Development
				'jwtkey'	=> '1sJWTk3y@2021'
			);

/* Client */
$client   = array(
				array(
					'client'		=> 'Aplikasi Android',
					'client_id'		=> 'ANDROID_SAMPLE',
					'client_secret'	=> '7621eb99983e23f0920b646ddd85aa7396ddd8a4'
				),
				array(
					'client'		=> 'Aplikasi iOS',
					'client_id'		=> 'IOS_SAMPLE',
					'client_secret'	=> 'ca82e6244786e78abe27a31d43406ed61cf69da4'
				),
			);

?>
