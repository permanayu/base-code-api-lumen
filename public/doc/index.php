<!DOCTYPE html>
<html>
<head>
	<title>Dokumentasi API</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="vendors/bulma-0.3.0/css/bulma.css">
	<link rel="stylesheet" type="text/css" href="vendors/font-awesome-4.7.0/css/font-awesome.min.css">

	<script type="text/javascript" src="jquery-3.1.0.min.js"></script>

	<style type="text/css">
		#response,
		#jwt{
			max-height: 800px;
			overflow-y: scroll;
			word-wrap: break-word;
			background-color: #f2f2f2;
			font-size: 12px;
		}
	</style>
</head>
<body>

<?php 
include "config.php"; 
include "endpoint.php"; 
?>

<nav class="nav has-shadow">
  	<div class="container">
    	<div class="nav-left">
      		<h1 class="title is-4" style="padding-top: 10px;">Dokumentasi API</h1>
	    </div>
	    <span class="nav-toggle">
	      	<span></span>
	      	<span></span>
	      	<span></span>
	    </span>
	    <div class="nav-right nav-menu">
	      	<!-- <a class="nav-item is-tab">Log out</a> -->
	    </div>
  	</div>
</nav>

<section class="section">
	<div class="container">
		<div class="columns">
			<div class="column is-3">
				<aside class="menu">
				  	<p class="menu-label">
				    	General
				  	</p>
				  	<ul class="menu-list">
				    	<li><a href="?" <?php if(!isset($_GET['page'])) echo "class='is-active'"; ?>>Mulai</a></li>
				    	<li><a href="?page=config" <?php if(@$_GET['page'] == 'config') echo "class='is-active'"; ?>>Konfigurasi</a></li>
				  	</ul>
				  	<p class="menu-label">
				    	Endpoint
				  	</p>
				  	<ul class="menu-list">
				  		<?php

				  		for ($i = 0; $i < sizeof($endpoint); $i++) {

				  			$active = isset($_GET['id']) && $_GET['id'] == $i ? "class='is-active'" : "";

				  			echo"<li><a href='?page=endpoint&id=". $i ."' $active>". $endpoint[$i]['menu'] ."</a></li>";
				  		}

				  		?>
				  	</ul>
				</aside>
			</div>
			<div class="column is-9">

				<?php

				switch (@$_GET['page']) {

					default:

						echo "
						<h1 class='title'>Mulai</h1>
						<h2 class='subtitle'>Cara memulai menggunakan API</h2>
						<hr>

						<p>Dalam melakukan request ada beberapa hal yang harus dimengerti dan dipersiapkan.</p>

						<br>
						<h3 class='title is-4'>API Request</h3>
						<hr>

						<h4 class='title is-4'>JWT</h4>
						<p>JWT (JSON Web Token) yaitu skema token yang digunakan untuk melindungi kemanandalam mengakses sebuah API. JWT tersedia dalam berbagai bahasa pemmograman,untuk lebihlengkapnya silahakan buka <a href='https://jwt.io'>https://jwt.io</a> </p> <br>
						<p>Dalam setiap JWT akan memiliki secret, yaitu sebuah keyword yang digunakan sebagai key untuk melakukan encode atau pun decode requet parameter. </p>

						<br>
						<br>
						<h3 class='title is-4'>API Response</h3>
						<hr>
						<h4 class='title is-4'>Success</h4>
						<p>Success response merupakan response dari api ketika request berhasil dilakukun. Berikut ini contoh dari success response:</p>

						<pre>
						{
							&nbsp; &nbsp; \"status\" : \"OK\",
							&nbsp; &nbsp; \"message\" : \"Success\",
							&nbsp; &nbsp; \"results\" : {},
							&nbsp; &nbsp; \"time\" : \"2017-01-07 12:01:02\"
						}
						</pre>
						<br>
						<p>Pada parameter results akan berisi object dari data yang telah berhasil direquest.</p>

						<br>
						<h4 class='title is-4'>Error</h4>
						<p>Error response merupakan response dari api yang salah, biasanya terjadi akibat kesalah dari  request atau pun sistem yang ada pada web service itu sendiri. Berikut ini contoh dari error response:</p>

						<br>
						<pre>
						{
							&nbsp; &nbsp; \"status\" : \"ERROR\",
							&nbsp; &nbsp; \"message\" : \"Undefined request parameter\",
							&nbsp; &nbsp; \"results\" : null,
							&nbsp; &nbsp; \"time\" : \"2017-01-07 12:01:02\"
						}
						</pre>

						<br>
						<p>Status yang dimiliki dari response tersebut yaitu ERROR dan juga memiliki pesan yang berkaitan dengan error yang terjadi.</p>
						
						<br>
						<h4 class='title is-4'>Invalid Auth</h4>
						<p>Response yang didapat ketika token yang digunakan oleh user telah berubah ataupun kadaluarsa. Berikut contoh dari invalid auth response :</p>
						<br>
						<pre>
						{
							&nbsp; &nbsp; \"status\" : \"INVALID_AUTH\",
							&nbsp; &nbsp; \"message\" : \"Invalid authentification\",
							&nbsp; &nbsp; \"results\" : null,
							&nbsp; &nbsp; \"time\" : \"2017-01-07 12:01:02\"
						}
						</pre>

						";

						break;

					case 'config':

						echo "
						<h1 class='title'>Konfigurasi</h1>
						<h2 class='subtitle'>Konfigurasi yang digunakan untuk API</h2>
						<hr>

						<p>Berikut ini adalah konfigurasi yang digunakan untuk melakukan request API.</p>
						<p>
						URL : ". $config['url'] ." <br>
						JWT Key : ". $config['jwtkey'] ."
						</p>

						<br>
						<h4 class='title is-4'>Klien</h4>
						";

						echo "
						<table class='table is-bordered is-striped is-narrow'>
							<tr>
								<th>No</th>
								<th>Klien</th>
								<th>Client id</th>
								<th>Client secret</th>
							</tr>
						";

						if (sizeof($client) == 0) {
							echo"<tr><td colspan='4' class='has-text-centered'>Tidak ada parameter</td></tr>";
						}
						else{

							$no = 1;

							for ($i = 0; $i < sizeof($client); $i++) {
								echo "
								<tr>
									<td>$no</td>
									<td>". $client[$i]['client'] ."</td>
									<td>". $client[$i]['client_id'] ."</td>
									<td>". $client[$i]['client_secret'] ."</td>
								</tr>
								";

								$no++;
							}
						}

						echo "</table>";

						break;

					case 'endpoint':

						$id 	= $_GET['id'];
						$title 	= $endpoint[$id]['menu'];
						$endp 	= $endpoint[$id]['endpoint'];
						$method = $endpoint[$id]['method'];
						$params = $endpoint[$id]['params'];

						echo "
						<h1 class='title'>$title</h1>
						<h2 class='subtitle'>Endpoint $title</h2>
						<hr>

						<p>
							Endpoint : $endp <br>
							Method : $method <br>
							Parameter : jwt <i>(jwt=[StringEncodeJWT])</i><br>
							Payload :
						</p>
						";

						echo "
						<br>
						<table class='table is-bordered is-striped is-narrow'>
							<tr>
								<th>No</th>
								<th>Kolom</th>
								<th>Keterangan</th>
								<th>Mandatory</th>
							</tr>
						";

						if (sizeof($params) == 0) {
							echo"<tr><td colspan='4' class='has-text-centered'>Tidak ada parameter</td></tr>";
						}
						else{

							$no = 1;

							for ($i = 0; $i < sizeof($params); $i++) {
								echo "
								<tr>
									<td>$no</td>
									<td>". $params[$i]['name'] ."</td>
									<td>". $params[$i]['desc'] ."</td>
									<td>". $params[$i]['mandatory'] ."</td>
								</tr>
								";

								$no++;
							}
						}

						echo "</table>";

						echo "
						<h2 class='subtitle'>Request</h2>
						<hr>

						<div class='field' style='margin-bottom:10px;'>
							<label class='label'>URL</label>
							<p class='control'>
								<input type='text' class='input' name='url' value='". $config['url'] . $endp ."' disabled>
							</p>
						</div>
						<div class='field' style='margin-bottom:10px;'>
							<label class='label'>Method</label>
							<p class='control'>
								<input type='text' class='input' name='method' value='$method' disabled>
							</p>
						</div>
						";

						if (sizeof($params) > 0) {

							for ($i = 0; $i < sizeof($params); $i++) {

								$value = "";

								if ($params[$i]['name'] == 'client_id') {
									$value = "value='". $client[0]['client_id'] ."'";
								}
								elseif ($params[$i]['name'] == 'client_secret') {
									$value = "value='". $client[0]['client_secret'] ."'";
								}

								$required = $params[$i]['mandatory'] == 'Y' ? "required='true'" : "";

								if ($params[$i]['name'] == 'photo' || $params[$i]['name'] == 'content' || $params[$i]['name'] == 'image' || $params[$i]['name'] == 'doc') {
									echo "
									<div class='field' style='margin-bottom:10px;'>
									  	<label class='label'>". $params[$i]['name'] ."</label>
									  	<p class='control'>
											<textarea class='input' name='". $params[$i]['name'] ."' placeholder='Masukan ". $params[$i]['name'] ."' $required></textarea>
									  	</p>
									</div>
									";
								}
								else{
									echo "
									<div class='field' style='margin-bottom:10px;'>
									  	<label class='label'>". $params[$i]['name'] ."</label>
									  	<p class='control'>
									    	<input class='input' type='text' name='". $params[$i]['name'] ."' placeholder='Masukan ". $params[$i]['name'] ."' $value $required>
									  	</p>
									</div>
									";
								}

								$no++;
							}

							echo "
							<div class='field is-grouped'>
							  	<p class='control'>
							    	<button class='button is-primary' id='request'>Send Request</button>
							  	</p>
							</div>
							";
						}

						echo "<br>";

						echo "
						<h2 class='subtitle'>JWT Request</h2>
						<hr>
						<div id='jwt'></div>
						";

						echo "<br>";

						echo "
						<h2 class='subtitle'>Response</h2>
						<hr>
						<pre id='response'></pre>
						";

						break;
				}

				?>

			</div>
		</div>
	</div>
</section>

<footer class="footer">
  	<div class="container">
	    <div class="has-text-centered">
	      <p>Copyright 2017. Yudi Setiadi Permana</p>
	    </div>
  	</div>
</footer>

</body>
</html>

<script type="text/javascript">
$(document).ready(function(){

 	$('#request').click(function(){

        var url 	= "request.php";
        var params 	= "";
        var error 	= false;

        $('.input').each(function(){

        	params = params + $(this).attr('name') + '=' + $(this).val() + '&';

        	if ($(this).val() == '' && $(this).attr('required') == 'true') {
        		$(this).addClass('is-danger');
        		error = true;
        	}

        });

        console.log(params);

        if (!error) {

        	$('#jwt').html('Waiting for request ...');
        	$('#response').html('Waiting for request ...');

        	$.ajax({
	            type : 'POST',
	            url : url,
				data : params,
	            dataType : 'JSON',
	            success : function(data) {
	                console.log(data);
	                $('#response').html(data.response);
	                $('#jwt').html(data.jwt);
	            },
	            error : function(XMLHttpRequest, textStatus, errorThrown) {
	                $('#response').html('Error : Request has been failed');
	            }
	        });
        }

    });

    $('.input').click(function(){
    	$(this).removeClass('is-danger');
    });

});
</script>
