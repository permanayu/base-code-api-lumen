<?php
/**
 * Configurasi document.
 *
 * @author Yudi Setiadi Permana
 */

/* Endpoint */
$endpoint = array(
    array(
        'menu' => 'Login',
        'endpoint' => '/user/login',
        'method' => 'POST',
        'encrypt' => 'JWT',
        'params' => array(
            array(
                'name' => 'client_id',
                'desc' => 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'client_secret',
                'desc' => 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'username',
                'desc' => 'Nama pengguna yang terlah terdaftar pada sistem',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'password',
                'desc' => 'Kata sandi yang digunakan untuk mengakses sistem informasi akademik',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'fcm_token',
                'desc' => 'Token dari smartphone yang akan didaftarkan pada FCM',
                'mandatory' => 'Y'
            )
        )
    ),
    array(
        'menu' => 'Profil',
        'endpoint' => '/user/profile',
        'method' => 'GET',
        'encrypt' => 'JWT',
        'params' => array(
            array(
                'name' => 'client_id',
                'desc' => 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'client_secret',
                'desc' => 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'token',
                'desc' => 'Token yang didapat pada saat proses login',
                'mandatory' => 'Y'
            )
        )
    ),
    array(
        'menu' => 'Logout',
        'endpoint' => '/user/logout',
        'method' => 'POST',
        'encrypt' => 'JWT',
        'params' => array(
            array(
                'name' => 'client_id',
                'desc' => 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'client_secret',
                'desc' => 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'token',
                'desc' => 'Token yang didapat pada saat proses login',
                'mandatory' => 'Y'
            )
        )
    ),
    array(
        'menu' => 'Notification',
        'endpoint' => '/notification',
        'method' => 'GET',
        'encrypt' => 'JWT',
        'params' => array(
            array(
                'name' => 'client_id',
                'desc' => 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'client_secret',
                'desc' => 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'token',
                'desc' => 'Token yang didapat pada saat proses login',
                'mandatory' => 'Y'
            )
        )
    ),
    array(
        'menu' => 'Notification Read',
        'endpoint' => '/notification/read',
        'method' => 'POST',
        'encrypt' => 'JWT',
        'params' => array(
            array(
                'name' => 'client_id',
                'desc' => 'Id client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'client_secret',
                'desc' => 'Secret client untuk mengakses API, diberikan webservice. Cek konfigurasi',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'token',
                'desc' => 'Token yang didapat pada saat proses login',
                'mandatory' => 'Y'
            ),
            array(
                'name' => 'notif_id',
                'desc' => 'ID notifikasi dari list',
                'mandatory' => 'Y'
            )
        )
    )
);
?>